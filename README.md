#Graphene Django Docker
This is just me trying to learn graphene with django while also using docker.
I wrote about this at https://medium.com/@john.nissenhooper/baby-steps-with-graphql-django-and-docker-b1319608c41 if you want to follow along

you need to have docker installed for this
after cloning run:
`docker-compose build`
`docker-compose run web python manage.py migrate`
`docker-compose run web python manage.py loaddata ingredients`
`docker-compose run web python manage.py createsuperuser`
and finally
`docker-compose up`
then navigate to [localhost:8000/graphql](http://localhost:8000/graphql)
